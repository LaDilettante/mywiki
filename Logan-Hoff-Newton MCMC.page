ww
: matrix of job characteristics (number of jobs $\times$ number of job characteristics). It's scaled by dividing by 10.

wa
: $[\sum_i w_{i1}, \sum_i w_{i2}]$, sample sum of the job characteristics 1 and 2

xx
: matrix of worker characteristics (number of workers $\times$ number of worker characteristics). It's scaled by dividing by 10.

eta
: `xx %*% beta`, matrix (2149 $\times$ 4) $\times$ (4 $\times$ 18) = 2149 $\times$ 18

eps1=0.02
: scale of alpha update (?)

eps2=1/20
: scale of beta update; reduction of sd (?)

Update parameters:

- Update $\alpha$: $\alpha^* \in \alpha \pm \epsilon_{\alpha}$
- Update avecstar: avecstar = $\exp(WW\alpha^*)$
- Update denstar: `denstar <- opp %*% avecstar`

Individual $i$, jobs $j \in {1, \dots, 18}$:

- avec: $[\exp(\alpha' ww_{1}), \dots, \exp(\alpha' ww_{18})]$
- den: $[\sum_{j: j \in O_1} \exp(\alpha' ww_j), \dots, \sum_{j: j \in O_i} \exp(\alpha' ww_j), \dots, \sum_{j: j \in O_{2149}} \exp(\alpha' ww_j) ]$

\begin{align}
&\frac{\prod_{i=1}^{2149} \sum_{j: j \in O_1} \exp(\alpha' ww_j)}{\prod_{i=1}^{2149} \sum_{j: j \in O_1} \exp(\alpha^{*\prime} ww_j)} \\
&(\sum_i w_{i1} \epsilon_{\alpha1} + \sum_i w_{i2} \epsilon_{\alpha2}) + \sum_{i = 1}^{2149} \log \sum_{j: j \in O_1} \exp(\alpha' ww_j) - \sum_{i = 1}^{2149} \log \sum_{j: j \in O_1} \exp(\alpha^{*\prime} ww_j)
\end{align}

- $\eta$ (2149 $\times$ 18): $\eta_1 (1 \times 18) = [\beta_1' XX_1, \dots, \beta_{18}' XX_{18}]$